variable "chart_version" {
    type = string
    description = "Chart version"
}

variable "docker_image" {
  description = "The Docker image to deploy"
  type        = string
}
