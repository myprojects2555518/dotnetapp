resource "helm_release" "release" {
  name       = "my-dotnetapp-release1"
  chart      = "dotnetapp"
  version    = var.chart_version

  set {
    name  = "image.repository"
    value = var.docker_image
  }

  values = [
    "${file("dotnetapp/values.yaml")}"
  ]
}