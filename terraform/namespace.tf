resource "kubernetes_namespace_v1" "my-ns" {
  metadata {

    labels = {
      app = "my-app"
    }

    name = "my-ns-app"
  }
}