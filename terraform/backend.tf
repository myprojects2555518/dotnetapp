terraform {
  backend "azurerm" {
    resource_group_name  = "LearnTech01"  
    storage_account_name = "learntechstorage"                     
    container_name       = "statefile"                  
    key                  = "dotnet-terraform.tfstate"    
  }
}
