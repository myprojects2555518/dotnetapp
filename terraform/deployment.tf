resource "kubernetes_deployment" "my-deployment" {
  metadata {
    name = "app-deployment"
    namespace = "my-ns-app"
    labels = {
      app = "my-app"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "my-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "my-app"
        }
      }

      spec {
        container {
          image = "learntechacr.azurecr.io/myrepo/sample:${var.image_version}"
          name  = "dotnet-app"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
  depends_on = [
    "resource.kubernetes_namespace_v1.my-ns"
  ]

}