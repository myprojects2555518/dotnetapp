resource "kubernetes_service" "my-service" {
  metadata {
    name = "my-service"
    namespace = "my-ns-app"
  }
  spec {
    selector = {
      app = "my-app"
    }
    session_affinity = "ClientIP"
    port {
      port        = 8080
      target_port = 80
    }

    type = "ClusterIP"
  }
}