resource "kubernetes_ingress_v1" "my-ingress" {
  metadata {
    name = "nginx-ingress"
    namespace = "my-ns-app"
    annotations = {
      "nginx.ingress.kubernetes.io/proxy-connect-timeout" = "180"
      "nginx.ingress.kubernetes.io/proxy-read-timeout" = "180"
      "nginx.ingress.kubernetes.io/proxy-send-timeout" = "180"
      "kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {
      rule {
        http {
         path {
           path = "/"
           backend {
             service {
               name = "my-service"
               port {
                 number = 80
               }
             }
           }
        }
      }
    }
  }
}